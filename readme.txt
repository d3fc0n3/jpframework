JP Framework based on Twitter Bootstrap
All the available classes and options in http://twitter.github.io/bootstrap/

1- What is this?
	* This is a Joomla! template framework to version 3.x

2- What is Joomla?
	* Joomla is a Content Management System (CMS) which enables you to build Web sites and powerful online applications.
	* It's a free and OpenSource software, distributed under the GNU General Public License version 2 or later
	* This is a simple and powerful web server application and it requires a server with PHP and either MySQL, PostgreSQL, or SQL Server to run it.
	More details here: http://www.joomla.org/about-joomla.html

5- Learn Joomla!
	* Read Getting Started with Joomla to find out the basics: http://docs.joomla.org/Getting_Started_with_Joomla!
	* Before installing, read the beginners guide: http://docs.joomla.org/Beginners

7- Is it easy to install?
	* Install the jpframework package as an usual extension
	* Select jpframework as a default template
	* Start to create content block in the jpframework component

9- Updates are free!
	* Always use the latest version: https://bitbucket.org/lazypdf/jpframework
